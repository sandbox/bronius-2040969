Lots of work to do yet.  Most especially:
Don't know why, but none of the ewie-gooey details of Date field (pre/processor, timezone treatment, etc) are getting inherited by this widget
And on that note, a minor hack to date.field.inc is required to avoid a PHP notice in a simple way:
        $date1 = new DateObject($item['value'], $item['timezone'], date_type_format($field['type']));
becomes:
        $timezone = !empty($item['timezone']) ? $item['timezone'] : date_default_timezone();
        $date1 = new DateObject($item['value'], $timezone, date_type_format($field['type']));
