(function($) {

Drupal.behaviors.dateFieldPickerUI = {
  attach: function (context, settings) {

    // Manage row-select visual effect
    var checkboxes = $('.field-widget-date-field-picker-ui-calendarux input.form-checkbox');
    checkboxes.change(function() {
      date_field_picker_ui_toggle_state(this);
    });

    $.each(checkboxes, function( key, value ) {
      if ($(this).is(':checked')) {
        date_field_picker_ui_toggle_state(this);
      }
      if ($(this).is(':disabled')) {
        date_field_picker_ui_show_disabled(this);
      }
    });
  }
};



})(jQuery);


function date_field_picker_ui_toggle_state(checkbox) {
  var span = jQuery(checkbox).parent().toggleClass('selected');
  var parentweekcontainer = jQuery(checkbox).parents('.form-type-checkbox').find('table.week-container').toggleClass('selected');
}

function date_field_picker_ui_show_disabled(checkbox) {
      console.log(checkbox);
  var parentweekcontainer = jQuery(checkbox).parents('.form-type-checkbox').find('table.week-container').addClass('disabled');
}

