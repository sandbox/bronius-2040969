<?php


function date_field_picker_ui_field_widget_info() {
  $settings = array(
    'settings' => array(
      'input_format' => date_default_format('date_text'),
      'input_format_custom' => '',
      'increment' => 15,
      'text_parts' => array(),
      'year_range' => '-3:+3',
      'label_position' => 'above',
    ),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'default value' => FIELD_BEHAVIOR_NONE,
    ),
  );

  $info = array(
    'date_field_picker_ui_calendarux' => array(
      'label' => t('Calendar UX'),
      'field types' => array('datestamp'),
     ) + $settings,
  );

  // The date text widget should use an increment of 1.
  $info['date_field_picker_ui_calendarux']['increment'] = 1;
  
  
  return $info;
}

function date_field_picker_ui_element_info() {
  
  $date_base = array(
    '#input' => TRUE, '#tree' => TRUE,
    '#date_timezone' => date_default_timezone(),
    '#date_flexible' => 0,
    '#date_format' => variable_get('date_format_short', 'm/d/Y - H:i'),
    '#date_text_parts' => array(),
    '#date_increment' => 1,
    '#date_year_range' => '-3:+3',
    '#date_label_position' => 'above',
  );
  $info['date_field_picker_ui_calendarux'] = array_merge($date_base, array(
    '#process' => array('date_text_element_process'),
//    '#theme_wrappers' => array('date_text'),
    '#value_callback' => 'date_text_element_value_callback',
  ));
  
  return $info;
}

function date_field_picker_ui_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = date("Y-m-d", strtotime('next week'));
  
  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {

    case 'date_field_picker_ui_calendarux':
      $fieldid = 'field_date_calendarux_' . $delta;
      $calendar_row_html_array = _date_field_picker_ui_build_calendar_html(_date_field_picker_ui_build_calendar_week($value, $delta));
      $calendar_row_html = $calendar_row_html_array['html'];
      $firstdate = $calendar_row_html_array['firstdate'];
      $widget += array(
        '#attributes' => array('class' => array('edit-field-date-calendarux')),
        '#id' => $fieldid,
        '#type' => 'checkbox',
        '#return_value' => strtotime($firstdate), // Convert to timestamp, even though some 'date_text' processor should do it on submission..
        '#attached' => array(
          'css' => array(drupal_get_path('module', 'date_field_picker_ui') . '/css/date_field_picker_ui.css'),
        ),
      );
      // Override some specific properties (whereas above array union favors original element..)
      // Per: https://api.drupal.org/comment/50238#comment-50238
      $widget['#title'] = $calendar_row_html;
      $widget['#draggable'] = FALSE;
      $widget['#settings']['timezone'] = date_default_timezone();
  }

  $element['value'] = $widget;
  return $element;
}

/**
 * Helper function to provide days of the week as an array
 * @param date $startdate Date representing the start of the week
 * @param int $week Week number, same as field $delta coming in
 * @return array
 */
function _date_field_picker_ui_build_calendar_week($startdate, $week) {
  $workdate = strtotime($startdate);
  $workdate = strtotime("+$week week", $workdate);
  $start = strtotime('last Sunday', $workdate);
  $finish = strtotime('this Saturday', $workdate);
  $days = array();
  while ($start <= $finish) {
    $days[] = array(
      'date' => date('j', $start),
      'month' => date('n', $start),
      'monthname' => date('M', $start),
      'fulldate' => date('Y-m-d', $start),
    );
    $start += strtotime('+1 day', 0);
  }
  return $days;
}

/**
 * Helper function to generate HTML representing one week of a monthly calendar
 * @param array $dates Array of integers representing dates of the week
 * @return string
 */
function _date_field_picker_ui_build_calendar_html($dates) {
  
  // Prepare table theme data row adding classes for month striping
  $firstdate = NULL;
  foreach($dates as $key => $datecontainer) {
    $firstdate = empty($firstdate) ? $datecontainer['fulldate'] : $firstdate;
    $date = $datecontainer['date'];
    $month = $datecontainer['month'];
    $monthname = $datecontainer['monthname'];
    $togglemonth = $month % 2 ? TRUE : FALSE;
    $dates[$key] = array(
      'data' => '<span class="calendar-ux-month">'.$monthname.' </span><span class="calendar-ux-date">'.$date.'</span>',
      'class' => array(
        $togglemonth ? 'even-month' : 'odd-month',
        $key == 0 || $date == 1 ? 'toggle-day' : '',
      ),
    );
  }

  // Format output as drupal themed table
  $rows = array(
    'data' => $dates
  );
  $calendar_html = theme('table', array('header' => array(), 'rows' => $rows,
    'attributes' => array(
      'class' => array('week-container'),
    ),
  ));
  return array(
    'html' => $calendar_html,
    'firstdate' => $firstdate,
  );
}